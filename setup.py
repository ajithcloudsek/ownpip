from setuptools import setup

setup(
    name='ownpip',
    version='0.0.2',
    author='ajithcloudsek',
    author_email='ajith.kumar@cloudsek.com',
    url='https://ajithcloudsek@bitbucket.org/ajithcloudsek/ownpip.git'
)